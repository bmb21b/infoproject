<!DOCTYPE html>
<!--
index.php
Author: Brian
-->
<?php
    $td = time();
    $datepp = date("Y/m/d", $td);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Personal info</title>
        <link rel="stylesheet" href="public/css/main.css" />
        <script src="public/jquery/jquery-1.js"></script>
        <script src="public/jquery/jquery-ui.js"></script>
        <link rel="stylesheet" href="public/jquery/jquery-ui.css" />
        <link rel="stylesheet" href="public/jquery/style.css">
        <script>
	    $( function() {
		    $( "#birthd" ).datepicker({
			    numberOfMonths: 1,
			    showButtonPanel: true,
                dateFormat: "yy/mm/dd"
		    });
            $( "#date" ).datepicker({
			    numberOfMonths: 1,
			    showButtonPanel: true,
                dateFormat: "yy/mm/dd"
		    });
	    } );
	    </script>
    </head>
    <body>
        <a href="views/listview.php?filterv=d&viewall=0&info=0">info</a>
        <hr>
        <form class="contact_us" method="post" name="contact_us" action="libs/Personalinfo.php">
                <fieldset>
                    <legend>Information</legend>
                </fieldset>
                <p></p>
                <div><output id="lblName"></output>
                    <input type="text" name="name" id="name" placeholder="First Name" required /><label  for="name">First Name:</label>
                </div>
                <p></p>
                <div><output id="lblSurname"></output>
                    <input type="text" name="surname" id="surname" placeholder="Surname" required /><label  for="surname">Surname:</label>
                </div>
                <p></p>
                <div><output id="lblBirthD"></output>
                    <input type="text" name="birthd" id="birthd" placeholder="Birth Date yyyy/mm/dd" required /><label  for="birthd">Birth Date:</label>
                </div>
                <p></p>
                <div><output id="lblCellN"></output>
                    <input type="text" name="celln" id="celln" placeholder="Cellphone Number" required /><label  for="celln">Cellphone Number:</label>
                </div>
                <p></p>
                <div><output id="lblEmail"></output>
                    <input type="email" name="email" id="email" placeholder="Email" required /><label for="email">Email:</label>
                </div>
                <p></p>
                <div><output id="lblDate"></output>
                    <input type="text" name="date" id="date" placeholder="Date Captured yyyy/mm/dd" value="<?php echo $datepp; ?>" required /><label  for="date">Date Captured:</label>
                </div>
                <p></p>
				<div class="actions">
                    <input type="submit" id="btnOK" value="OK" />
	            </div>
                <p></p>
		    </form>
    </body>
</html>
