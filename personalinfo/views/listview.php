<!DOCTYPE html>
<!--
listview.php
Author: Brian
-->
<?php
    include '../libs/Info.php';
    $app = new Info("vlist");
    $info = $app->infoList();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>List Views</title>
    </head>
    <body>
        <?php
            
            $fc = $info[0];
            $nc = $info[1];
            $offset = $nc + 2;
            $p = $nc/10;
            $infoA = [];
            echo "<br>", $fc, " => ", $nc, " : ",$p, "<br>";
            for ($j = 2; $j < $offset; $j++ )
            {
                echo "[ ", $j, ' ]<br>';
                $rsa = $info[$j];
                for ($i = 0; $i < $fc; $i++)
                {
                    $infoA[] = $rsa[$i];
                    echo $i," ", $rsa[$i], '<br>';
                }
                $infoA[] = ';';
            }
            echo  $infoA[0],"<br>";
            $inf = ',';
            $inf2 = implode($inf, $infoA);
            header("Location: view.php?list=".$inf2.",inf=0");
            exit();
        ?>
    </body>
</html>
