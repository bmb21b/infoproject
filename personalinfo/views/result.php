<!DOCTYPE html>
<!--
result.php
Author: Brian
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Results</title>
        <link rel="stylesheet" href="../public/css/main.css" />
        <script src="../public/jquery/jquery-1.js"></script>
        <script type="text/javascript" lang="javascript">
            $(document).ready(function(){
                //alert("DR");
                $("#btnResults").click(function(){
                    //alert("btnC");
                    $("#results").load("../libs/Info.php",{"viewall":"viewall"});
                });
            });
        </script>
    </head>
    <body>
        <h1>Views</h1>
        <div>
            <?php
            include '../libs/Info.php';
            $app = new Info("vlist");
            $info = $app->infoList();
            //header("Location: listview.php?list=".$info);
            //exit();
            $fc = $info[0];
            $nc = $info[1];
            $p = $nc/10;
            echo $fc, " => ", $nc, "<br>";
            echo '<table cellpadding=\"3\" cellspacing=\"1\" border=\"1\"><caption>'.$p.'</caption>';
            for ($j = 2; $j < $nc; $j++ )
            {
                echo '<tr>';
                $rsa = $info[$j];
                for ($i = 0; $i < $fc; $i++)
                {
                    echo '<td>'.$rsa[$i].'</td>';
                }
                echo '</tr>';
            }
            echo '</table>';
            echo '<hr>';
            ?>
        </div>
        
        <div id="results" style="background-color: #ccccff">
            Results
        </div>
        <input type="button" id="btnResults" value="Views"/>
    </body>
</html>
