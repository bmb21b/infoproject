<!DOCTYPE html>
<!--
view.php
Author: Brian
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Views</title>
        <link rel="stylesheet" href="../public/css/main.css" />
        <script src="../public/jquery/jquery-1.js"></script>
        <script src="../public/js/app.js"></script>
    </head>
    <body>
        <div>
            <a href="../index.php">Exit</a>
            <input type="button" id="btnBack" value="Back"/>
            <input type="button" id="btnMore" value="Next"/>
            <select id="selView">
                <option>Filter Views</option>
                <option value="firstname">First Name</option>
                <option value="surname">Surname</option>
                <option value="birthdate">Date of Birth</option>
                <option value="datecaptured">Date Captured</option>
            </select>
        </div>
        <hr>
        <h1>Views</h1>
        <div id="pages" style="background-color: #ccccff"></div>
        <div id="results">
            <table id="tblView" cellpadding="3" cellspacing="1" border="1"></table>
            <p id="tblData"></p>
            <p id="backView"></p>
        </div>
    </body>
</html>
