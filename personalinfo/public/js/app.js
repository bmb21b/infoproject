/* 
 * app.js
 * Author: Brian
 */
var listp;
var page = 0;
var pages = 0;
var listv;
var line = 0;
var max = 0;
var urlList;
//var tablehd, tabled;
var tableth;
var plist;
var view = 0;

function init(){
    //alert("init");
    var aUrl = document.URL;
    urlList = aUrl.split("?list=");
    var list = urlList[1].split(",inf=");
    listp = list[0];
    listv = listp.split(';,');
    max = listv.length;
    pages = Math.ceil(max/10);
    //tablehd = '<table id="tblView" cellpadding="3" cellspacing="1" border="1">';
    tableth = "<tr><th>ID</th><th>FirstName</th><th>Surname</th><th>BirthDate</th><th>CellphoneNumber</th><th>Email</th><th>DateCaptured</th></tr>";
    //tabled = "</table>";
    $("#tblView").append($("#tblData"));
    $("#tblView").append($("#backView"));
    plist = [];
    listView();
    view = 0;
    listNextview(view);
}

$(document).ready(function(){
    //alert("docrd");
    $("#btnBack").click(function(){
        var pageBck = view;
        $("#results").hide("slow");
        $("#results").show("slow");
        $("#pages").hide("slow");
        $("#pages").show("slow");
        listBackview(pageBck);
    });
    
    $("#btnMore").click(function(){
        var pageNxt = view;
        $("#pages").hide("slow");
        $("#pages").show("slow");
        $("#results").hide("slow");
        $("#results").show("slow");
        listNextview(pageNxt);
    });
    
    $("#selView").change(function(){
        var selv = $("#selView").val();
        //alert(selv);
        var loc = urlList[0].split("view.p");
        window.location = loc[0]+"listview.php?viewall=0&filterv="+selv+"&info=0";
    });
});

function listView(){
    var data;
    var dl;
    var data2;
    var pagev = " ";
    for(var i = 0; i < 10; i++){
        if(line < max){
            line++;
            data = listv[line - 1];
            data2 = data.split(',');
            dl = data2.length;
            pagev += "<tr>";
            for(var j = 0; j < dl; j++){
                pagev += "<td> "+data2[j]+" </td>";
            }
            pagev += "</tr>";
        }   
    }
    if(page < pages){
        plist.push(pagev);
        page++;
        listView();
    }
}

function listNextview(next){
    if(next < pages){
        $("#tblData").text(" ");
        $("#tblData").append(tableth);
        view++;
        $("#pages").text("page "+view+"/"+pages);
        if(plist.length > 0){
            $("#tblData").append(plist[next]);
        }
        $("#backView").text("View "+view);
    }   
}

function listBackview(back){
    if(back > 1){
        $("#backView").text(" ");
        $("#backView").append(tableth);
        view--;
        $("#pages").text("page "+view+"/"+pages);
        if(plist.length > 0){
            $("#backView").append(plist[view - 1]);
        }
        $("#tblData").text("View "+view);
    }
}

window.onload = function(){
    init();
};
