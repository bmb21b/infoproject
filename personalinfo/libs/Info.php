<?php

/*
 * Info.php
 */
include '../configs/dbconfig.php';
//if ($_REQUEST["viewall"]){
//    $app = new Info("a");
//}

/**
 * Description of Info
 *
 * @author Brian
 */
class Info {
    var $pinfo;
    var $filterv;
    
    function __construct($a) 
    {
        $this->pinfo = [];
        $this->filterv = filter_var($_REQUEST["filterv"], FILTER_SANITIZE_STRIPPED);
        if($a == "a"){
           //$this->printUsers();
            echo "a <br>";
        }
    }
    
    function infoList(){
        $query = " ";
        echo $this->filterv;
        if($this->filterv == "d"){
            $query = "SELECT * FROM user";
        }
        else {
            $query = "SELECT * FROM user ORDER BY ".$this->filterv;
        }
        
        $this->infoX($query);
        return $this->pinfo;
    }
    
    function printUsers(){
        $query = "SELECT * FROM user";
        echo '<table cellpadding=\"3\" cellspacing=\"1\" border=\"1\"><caption>'.$query.'</caption>';
        echo '<tr><th>ID</th><th>FirstName</th><th>Surname</th><th>BirthDate</th><th>CellphoneNumber</th><th>Email</th><th>DateCaptured</th></tr>';
        $this->infoX($query);
        $this->printList();
        echo '</table>';
    }
    private function printList(){
        $fc = $this->pinfo[0];
        $nc = $this->pinfo[1];
        $ofset = $nc + 2;
        for ($j = 2; $j < $ofset; $j++ )
        {
            echo '<tr>';
            $rsa = $this->pinfo[$j];
            for ($i = 0; $i < $fc; $i++)
            {
                echo '<td>'.$rsa[$i].'</td>';
            }
            echo '</tr>';
        }
    }
    private function infoX($query)
    {
        try 
        {
            include_once '../configs/dbconn.php';
            $mysqli = connDB();
            $smt = $mysqli->prepare($query);
            $smt->execute();
            $rs = $smt->get_result();
            $fc = $rs->field_count;
            $nc = $rs->num_rows;
            $this->pinfo[] = $fc;
            $this->pinfo[] = $nc;
            for ($j = 0; $j < $nc; $j++ )
            {
                $rsa = $rs->fetch_array();
                $this->pinfo[] = $rsa;
            }
            $smt->close();
            $mysqli->close();
        } 
        catch (Exception $ex) 
        {
            echo "<br> EXCEPT:....... <br>";
            echo "$ex->getMessage()";            
        } 
    }   
}