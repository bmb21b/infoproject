<?php

/*
 * Personalinfo.php
 */

include '../configs/dbconfig.php';
$app = new Personalinfo($_REQUEST);
/**
 * Description of Personalinfo
 *
 * @author Brian
 */
class Personalinfo {
    var $name;
    var $surname;
    var $birthdate;
    var $cellphonen;
    var $email;
    var $datecap;
    var $datep;
    var $comment;
    var $trans;
    var $registerd;
    
    function __construct($info) {
        $this->name = filter_var($info["name"], FILTER_SANITIZE_STRIPPED);
        $this->surname = filter_var($info["surname"], FILTER_SANITIZE_STRIPPED);
        $this->birthdate = filter_var($info["birthd"], FILTER_SANITIZE_STRIPPED);
        $this->cellphonen = filter_var($info["celln"], FILTER_SANITIZE_STRIPPED);
        $this->email = filter_var($info["email"],FILTER_VALIDATE_EMAIL);
        $this->datecap = filter_var($info["date"], FILTER_SANITIZE_STRIPPED);
        $this->datep = time();
        $this->comment = 'New user.';
        $this->trans = 0;
        $this->findx($this->email);
        $this->registerp();
    }
    
    function registerp()
    {
        echo '<table cellpadding=\"3\" cellspacing=\"1\" border=\"1\"><caption>'.$this->datep.'</caption>';
        try 
        {
            include_once '../configs/dbconn.php';
            $mysqli = connDB();
            echo '<tr><td> host: '.$mysqli->host_info.'</td></tr>';
            echo '<tr><td> Conn#: '.$mysqli->server_info.'</td></tr>';
            echo '<tr><td> Conn#: '.$this->datecap.'</td></tr>';
            $datepp = date("Y/m/d H:i:s", $this->datep);
            $dateob = $this->dateFormat();
            echo '<tr><td> Dateob1#: '.$this->birthdate.'</td></tr>';
            echo '<tr><td> Datep#: '.$datepp.'</td></tr>';
            $query = "INSERT INTO user(firstname,surname,birthdate,cellphonenumber,email,datecaptured)"
                     ."VALUES(?,?,?,?,?,?)";
            $smt = $mysqli->prepare($query);
            $smt->bind_param("ssssss", $this->name, $this->surname, $dateob, $this->cellphonen, $this->email, 
                    $datepp);
            $this->registerd = $smt->execute();
            $smt->close();
            $mysqli->close();
            echo '<tr><td> Registerd: '.$this->registerd.'</td></tr>';
            echo '<tr><td><a href="../index.php">Exit3</a></td></tr>';
        } 
        catch (Exception $ex) 
        {
            echo '<tr><td>'.$ex->getMessage().'</td></tr>';
            echo '<tr><td><a href="../index.php">Exit4</a></td></tr>';            
        }
        echo '</table';
    }
    
    function findx($xuser)
    {
        $this->user = $xuser;
        try 
        {
            $useri = "";
            $usern = "";
            include_once '../configs/dbconn.php';
            $mysqli = connDB();
            $query = "SELECT id, firstname FROM user WHERE email = ?";
            $smt = $mysqli->prepare($query);
            $smt->bind_param("s", $this->user);
            $info = $smt->execute();
            $resultb = $smt->bind_result($useri, $usern);
            $fetch = $smt->fetch();
            $smt->close();
            $mysqli->close();
            $findp[] = $info;
            $findp[] = $resultb;
            $findp[] = $fetch;
            echo "0 exec: $findp[0] <br>";
            echo "1 result: $findp[1] <br>";
            echo "2 fetch: $findp[2] <br>";
            echo "usrid: $useri <br>";
            echo "usrname: $usern <br>";
            if($useri == "")
            {
                echo "$this->comment <br>";
                $this->trans = 1;
            }
            else if($this->trans === 0)
            {
                echo " $this->user is already registered <br>";
                echo '<tr><td><a href="../index.php">Exit4</a></td></tr>';
                //header("Location: ../index.php");
                exit();                    
            }
        } 
        catch (Exception $ex) 
        {
            echo '<tr><td>'.$ex->getMessage().'</td></tr>';
        }
    }
    
    function dateFormat(){
        $dateob = date($this->birthdate);
        $dateob2 = explode("/", $dateob);
        $dl = count($dateob2);
        $dfrm = "";
        if($dl > 2){
            if($dateob2[2] > $dateob2[0]){
                $dfrm = $dateob2[2]."/".$dateob2[0]."/".$dateob2[1];
                echo '<tr><td> Dateob2#: '.$dfrm.'</td></tr>';
                return $dfrm;
            }
        }
        echo '<tr><td> Dateob#: '.$dateob.'</td></tr>';
        return $dateob;
    }
    
}
