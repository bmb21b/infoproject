#///////////////////////////////////
#/* 
# *  personalinfo.sql
# */
#/**
# * Author:  Brian
# * 
# */
#////////////////////////////////////////



CREATE DATABASE PERSONALINFO;

USE PERSONALINFO;

CREATE TABLE USER (
id int NOT NULL auto_increment,
firstname varchar (30) NOT NULL,
surname varchar (30) NOT NULL,
birthdate date NOT NULL,
cellphonenumber varchar (30) NOT NULL,
email varchar (30) NOT NULL,
datecaptured datetime NOT NULL default '0000/00/00 00:00:00',
PRIMARY KEY (id)
);

GRANT ALL ON personalinfo.* to perdb@localhost identified by 'per3pw';
