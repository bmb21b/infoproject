delimiter $$

CREATE DATABASE `personalinfo` /*!40100 DEFAULT CHARACTER SET utf8 */$$

delimiter $$

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(30) NOT NULL,
  `surname` varchar(30) NOT NULL,
  `birthdate` date NOT NULL,
  `cellphonenumber` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `datecaptured` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8$$


GRANT ALL ON personalinfo.* to perdb@localhost identified by 'per3pw';



