/*
-- Query: SELECT * FROM personalinfo.user
LIMIT 0, 1000

-- Date: 2019-03-01 00:37
*/
INSERT INTO `user` (`id`,`firstname`,`surname`,`birthdate`,`cellphonenumber`,`email`,`datecaptured`) VALUES (1,'Cool','Cloo','2000-09-06','0123456789','cool@cool.com','2019-02-28 07:18:33');
INSERT INTO `user` (`id`,`firstname`,`surname`,`birthdate`,`cellphonenumber`,`email`,`datecaptured`) VALUES (2,'Deel','Ceel','2001-08-03','0987654321','deel@deel.com','2019-02-28 10:17:28');
INSERT INTO `user` (`id`,`firstname`,`surname`,`birthdate`,`cellphonenumber`,`email`,`datecaptured`) VALUES (3,'Rotorola','Qwe','1996-01-01','0787654321','roto@roto.com','2019-02-28 11:31:34');
INSERT INTO `user` (`id`,`firstname`,`surname`,`birthdate`,`cellphonenumber`,`email`,`datecaptured`) VALUES (4,'Qwer','Wer','2015-03-24','1230987770','qwert@q.co','2019-02-28 12:16:56');
INSERT INTO `user` (`id`,`firstname`,`surname`,`birthdate`,`cellphonenumber`,`email`,`datecaptured`) VALUES (5,'Coopqw','Peq','2014-10-13','6367890123','peq@qep.pm','2019-02-28 13:07:51');
INSERT INTO `user` (`id`,`firstname`,`surname`,`birthdate`,`cellphonenumber`,`email`,`datecaptured`) VALUES (6,'Vela','Bela','2018-10-18','3456789012','vela@vela.ve','2019-02-28 13:11:29');
INSERT INTO `user` (`id`,`firstname`,`surname`,`birthdate`,`cellphonenumber`,`email`,`datecaptured`) VALUES (7,'Lop','Loper','2017-12-26','5432109876','lop@lop.lp','2019-02-28 13:12:37');
INSERT INTO `user` (`id`,`firstname`,`surname`,`birthdate`,`cellphonenumber`,`email`,`datecaptured`) VALUES (8,'Xela','Fea','2018-03-22','4561237890','xela@xela.xom','2019-02-28 13:13:51');
INSERT INTO `user` (`id`,`firstname`,`surname`,`birthdate`,`cellphonenumber`,`email`,`datecaptured`) VALUES (9,'Hola','Lah','2017-11-15','0987123421','hola@hola.la','2019-02-28 13:15:13');
INSERT INTO `user` (`id`,`firstname`,`surname`,`birthdate`,`cellphonenumber`,`email`,`datecaptured`) VALUES (10,'See','Ees','2017-11-29','6782340989','see@se.sa','2019-02-28 13:16:19');
INSERT INTO `user` (`id`,`firstname`,`surname`,`birthdate`,`cellphonenumber`,`email`,`datecaptured`) VALUES (11,'Qrer','Req','2018-10-10','0101010123','qrer@qe.qo','2019-02-28 13:18:02');
INSERT INTO `user` (`id`,`firstname`,`surname`,`birthdate`,`cellphonenumber`,`email`,`datecaptured`) VALUES (12,'Gool','Yel','2012-09-23','2341237890','gool@gool.som','2019-02-28 13:19:53');
INSERT INTO `user` (`id`,`firstname`,`surname`,`birthdate`,`cellphonenumber`,`email`,`datecaptured`) VALUES (13,'Top','Wert','2008-04-09','2008901234','top@tp.po','2019-02-28 13:21:26');
